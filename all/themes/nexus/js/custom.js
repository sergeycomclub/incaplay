jQuery(window).load(function() {

  jQuery(".button_grid").click(function(){
    jQuery("body").toggleClass("grid");
});

        // Create mobile element
        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }

        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
        is_opera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
        is_ie     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

        if ( ( is_webkit || is_opera || is_ie ) && 'undefined' !== typeof( document.getElementById ) ) {
          var eventMethod = ( window.addEventListener ) ? 'addEventListener' : 'attachEvent';
          window[ eventMethod ]( 'hashchange', function() {
           var element = document.getElementById( location.hash.substring( 1 ) );

           if ( element ) {
            if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
             element.tabIndex = -1;

         element.focus();
     }
 }, false );
      }
      ;(function($){
        $("h2.title").each(
            function(){
                text = $(this).text();
                text_length = text.length;
                new_text = '';
                for (var i = 0; i < text_length; i++) {
                    if (text[i] == '0' || text[i] == '1' || text[i] == '2' || text[i] == '3' || text[i] == '4' || text[i] == '5' ||
                        text[i] == '6' ||text[i] == '7' ||text[i] == '8' ||text[i] == '9' ){
                            new_text = new_text + '<span style="font-family:initial; font-style:italic;">'+ text[i]+'</span>';
                    } else {

                    new_text = new_text + text[i];
                    }
            }
            $(this).html(new_text);
        }
        );
$("h1.title").each(
            function(){
                text = $(this).text();
                text_length = text.length;
                new_text = '';
                for (var i = 0; i < text_length; i++) {
                    if (text[i] == '0' || text[i] == '1' || text[i] == '2' || text[i] == '3' || text[i] == '4' || text[i] == '5' ||
                        text[i] == '6' ||text[i] == '7' ||text[i] == '8' ||text[i] == '9' ){
                            new_text = new_text + '<span style="font-family:initial; font-style:italic; font-size:inherit; color:inherit !important">'+ text[i]+'</span>';
                    } else {

                    new_text = new_text + text[i];
                    }
            }
            $(this).html(new_text);
        }
        );

    })(jQuery);
});