var $locale = jQuery('html').attr('lang');

function ColorLuminance(color, percent) {
    var num = parseInt(color.slice(1), 16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R < 255 ? R < 1 ? 0 : R : 255) * 0x10000 + (G < 255 ? G < 1 ? 0 : G : 255) * 0x100 + (B < 255 ? B < 1 ? 0 : B : 255)).toString(16).slice(1);
}
addressHref = window.location.href;


if (jQuery("#main-content").hasClass("article")) {
    jQuery('#main-menu > ul>li:nth-child(7)').addClass('active-trail')
}
var main_sub_menu = jQuery("#main_sub_menu");
var main_sub_menu_content = jQuery('#main-menu > ul>li.active-trail').html();
main_sub_menu.append(main_sub_menu_content);
jQuery(".footer-block section:nth-child(2)").click(function () {
    jQuery(".footer-block section:first-child").toggle(slow);
});
var menuItemNum = jQuery("#main-menu ul.menu.nav.nav-pills > li.active-trail").index();
var main_content = jQuery("#main-content");
var startColor = "rgb(196, 158, 109)";
var stopColor = "rgb(150, 116, 83)";
var buttonColor = "#907554";
var h1Color;
rezerwacje = '/rezerwacje';

if (addressHref == 'http://incaplay.pl/rezerwacje'
    || addressHref == 'http://incaplay.pl/pl/rezerwacje'
    || addressHref == 'http://incaplay.pl/en/rezerwacje') {
    menuItemNum = 8;
}

if (addressHref == 'http://incaplay.pl/pl/inca-cafe'
    || addressHref == 'http://incaplay.pl/inca-cafe'
    || addressHref == 'http://incaplay.pl/en/inca-cafe'
    || addressHref == 'http://incaplay.pl/pl/menu'
    || addressHref == 'http://incaplay.pl/menu'
    || addressHref == 'http://incaplay.pl/en/menu') {
    menuItemNum = 7;
}

if (jQuery("#main-content").hasClass("article")) {
    menuItemNum = 6;
}
if (addressHref.indexOf(rezerwacje) > -1) {
    menuItemNum = 4;
}

console.log(menuItemNum);
switch (menuItemNum) {
    case 0:
        buttonColor = "#30897f";
        startColor = "#53b496";
        stopColor = "#389175";
        hoverColor = "#055D53";
        break
    case 8 :
        buttonColor = "#545c9c";
        startColor = "rgb(115, 122, 210)";
        stopColor = "rgb(80, 87, 156)";
        darker = ColorLuminance("#6269b7", "10")
        jQuery("#block-system-main button").css("background-color", "darker");
        jQuery("#block-system-main button").css("border-color", "darker");
        hoverColor = "#1C2354";

        break


    case 2:
        startColor = "#66BBED";
        stopColor = "#489FE5";
        buttonColor = "#4093d8";
        hoverColor = "#10548C";

        break

    case 3:
        startColor = "#B0BCBC";
        stopColor = "#939D9E";
        buttonColor = "#869494";
        jQuery("#color_button").css("background-color", "red !important");
        jQuery("#color_button").css("border-color", "red !important");
        hoverColor = "#4B5252";


        break

    case 4:
    case 1:
        darker = ColorLuminance("#7cb338", "0.5");
        console.log(darker);
        startColor = "rgb(144, 200, 76)";
        stopColor = "rgb(103, 158, 35)";
        buttonColor = "#6a953a";
        hoverColor = "#34540F";
        h1Color = '#99cc00';


        break

    case 5:
        startColor = "rgb(245, 85, 154)";
        stopColor = "rgb(236, 0, 139)";
        buttonColor = "#545c9c";
        hoverColor = "#222A69";
        break

    case 6:

        startColor = "rgb(251, 0, 202)";
        stopColor = "rgb(200, 0, 236)";
        buttonColor = "rgb(91, 0, 179)";
        hoverColor = "#32035F";

        break

    case 7:
        startColor = "rgb(196, 158, 109)";
        stopColor = "rgb(150, 116, 83)";
        buttonColor = "#907554";
        hoverColor = "rgb(78, 58, 34)";

        break


    default:
        buttonColor = "#30897f";
        startColor = "#53b496";
        stopColor = "#389175";

}
if (typeof h1Color == 'undefined') {
    h1Color = buttonColor
}
jQuery(".color-block").css("border-color", stopColor + " transparent transparent transparent");
main_content.css("background-image", "repeating-linear-gradient(to bottom," + startColor + " 0%, " + stopColor + " 100%)");
// main_content.css("background-colorh1Color stopColor);

//    // main_content.css("background-image", "url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxIDEiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxsaW5lYXJHcmFkaWVudCBpZD0idnNnZyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSIwJSIgeTE9IjAlIiB4Mj0iMCUiIHkyPSIxMDAlIj48c3RvcCBzdG9wLWNvbG9yPSIjOTBjODRjIiBzdG9wLW9wYWNpdHk9IjEiIG9mZnNldD0iMCIvPjxzdG9wIHN0b3AtY29sb3I9IiM2NzllMjMiIHN0b3Atb3BhY2l0eT0iMSIgb2Zmc2V0PSIxIi8+PC9saW5lYXJHcmFkaWVudD48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI3ZzZ2cpIiAvPjwvc3ZnPg==)");
//    main_content.css("background-image", "-webkit-gradient(linear, 0% 0%, 0% 100%,color-stop(0, "+colorStart+"),color-stop(1, "+stopColor+"))");

//    main_content.css("background-image", "-webkit-repeating-linear-gradient(top,"+startColor+" 0%,"+stopColor+" 100%)");

//    main_content.css("background-image", "-ms-repeating-linear-gradient(top,"+startColor+" 0%,"+stopColor+" 100%)");
jQuery("#color_button").css('background-color', buttonColor);
jQuery("#color_button").css('border-color', buttonColor);
jQuery("div#main_sub_menu > .nav-pills > li>a").css('background-color', buttonColor);
jQuery("div#main_sub_menu > ul.nav-pills > li").hover(
    function () {
        jQuery(this).css('background-color', hoverColor);
        jQuery(this).children('a').css('background-color', hoverColor);
    },
    function () {
        jQuery(this).css('background-color', buttonColor);
        jQuery(this).children('a').css('background-color', buttonColor);
    });
jQuery("div#main_sub_menu > ul.menu > li > ul>li>a:focus").css('background-color', buttonColor);
jQuery("div#main_sub_menu > ul.menu > li > ul>li>a.active").css('color', "#C0FF00 ");
jQuery(".submenu-button:active").css('color', "#C0FF00 ");
jQuery(".submenu-button").css('background-color', buttonColor);
jQuery(".submenu-button:focus").css('background-color', buttonColor);
jQuery("#third_menu_block h2").css('color', buttonColor);
jQuery("#third_menu_block h1").css('color', h1Color);
jQuery("#third_menu_block h4").css('color', buttonColor);
jQuery("div.cafe_menu th").css('background-color', buttonColor);
jQuery("article h2.title a").css('color', buttonColor);
jQuery("article h1.title a").css('color', h1Color);
if (typeof hoverColor !== 'undefined') {

    jQuery("div#main_sub_menu > ul.menu > li > ul").css('background-color', hoverColor);
}


jQuery(window).scroll(function (event) {
    var scroll = jQuery(window).scrollTop();
    if (scroll > 200) {
        jQuery("#sticky-menu").css({top: '0px'});
    }
    if (scroll < 200) {
        jQuery("#sticky-menu").css({top: '-80px'});
    }


});
if (window.location.href == 'http://www.incaplay.pl/gallery'
    || window.location.href == 'http://www.incaplay.pl/en/gallery'
    || window.location.href == 'http://www.incaplay.pl/pl/gallery') {

    jQuery('.slider-wrapper.col-md-6.col-sm-12').removeClass('col-md-6');
}
jQuery(".fa-bars").click(
    function () {
        jQuery("#main-menu").toggle("show");
    });
if (addressHref.indexOf("?q=pl") > -1) {
    jQuery('.webform-confirmation + .links > a').text(' Powrót');
}
jQuery(document).ready(function ($) {


    now = new Date();
    nowDay = now.getDay();
    nowHour = now.getHours();
    nowMinutes = now.getMinutes();

    console.log(hide_rezerwacje_today(nowDay, nowHour, nowMinutes));


    function hide_rezerwacje_today(nowDay, nowHour, nowMinutes) {

        if (typeof(nowDay) == 'undefined'
            && typeof(nowMinutes) == 'undefined'
            && typeof(nowHour) == 'undefined'
        ) return;
        fromDay = 2;
        fromHour = 11;
        fromMinutes = 0;

        toDay = 5;
        toHour = 17;
        toMinutes = 30;

        console.log(nowDay, 'nowDay');

        switch (nowDay) {

            case 1: {
                return false;
                break;

            }
            case 3:
            case 4: {
                return true;
                break;
            }

            case 2: {
                if (nowHour >= fromHour) {
                    return true;
                }
                else {
                    return false;
                }
                break;
            }

            case 5: {

                if (nowHour < toHour) {
                    return true;
                }
                else {

                    if (nowHour == toHour && nowMinutes < toMinutes) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                break;
            }

            case 6:
            case 0: {
                console.log('order hour:', toHour);
                if (nowHour == 11 || nowHour == 14 || nowHour == 17) {
                    return true;
                }
                else {
                    return false;
                }
                break;

            }
        }
    }


    function setHourSelectHtml($timeText) {

        var day = new Date(year, month, date).getDay();
        if (day == 6 || day == 0) {
            return '<option value="" selected="selected">'
                + $timeText
                + '</option>'
                + '<option value="11">11</option>'
                + '<option value="14">14</option>'
                + '<option value="17">17</option>';
        } else {
            return '<option value="" selected="selected">'
                + $timeText
                + '</option><option value="11">11</option>'
                + '<option value="12">12</option>'
                + '<option value="13">13</option>'
                + '<option value="14">14</option>'
                + '<option value="15">15</option>'
                + '<option value="16">16</option>'
                + '<option value="17">17</option>';
        }
    }

    function setMinutesSelectHtml($timeText) {
        $timeText = $timeText || 'minute';
        console
        var day = new Date(year, month, date).getDay();

        if (day == 6 || day == 0) {
            return '<option value="" selected="selected">' +
                $timeText +
                '</option>' +
                '<option value="0">00</option>';
        } else {
            return '<option value="" selected="selected">' +
                $timeText +
                '</option>' +
                '<option value="0">00</option>' +
                '<option value="30">30</option>';
        }
    }

    function makeHourSelectElement() {

        if ($locale == 'pl') {
            jQuery('.contact-form-wrapper #edit-submitted-czas-hour').html(setHourSelectHtml('Godzina'));
            jQuery('.contact-form-wrapper #edit-submitted-czas-minute').html(setMinutesSelectHtml());
        } else {
            jQuery('.contact-form-wrapper #edit-submitted-time-hour').html(setHourSelectHtml('Hour'));
            jQuery('.contact-form-wrapper #edit-submitted-time-minute').html(setMinutesSelectHtml());
        }
    }

    if ($locale == 'pl') {
        $mondayText = 'IncaPlay jest nieczynna w poniedziałki - zapraszamy do rezerwacji w pozostałe dni tygodnia';
        $warningText = 'Wybrana godzina jest niedostępna prosimy o wybranie innej godziny';

        var $month = jQuery('#edit-submitted-data-przyjcia-urodzinowego-month');
        var $date = jQuery('#edit-submitted-data-przyjcia-urodzinowego-day');
        var $year = jQuery('#edit-submitted-data-przyjcia-urodzinowego-year');
        var $hour = jQuery('#edit-submitted-czas-hour');
        var $minutes = jQuery('#edit-submitted-czas-minute');
        var $datePartySection = jQuery('#webform-component-data-przyjcia-urodzinowego');
    }
    else {
        $mondayText = 'We are not working at Monday!';
        $warningText = 'We are not working this day/time';

        var $month = jQuery('#edit-submitted-date-of-party-month');
        var $date = jQuery('#edit-submitted-date-of-party-day');
        var $year = jQuery('#edit-submitted-date-of-party-year');
        var $hour = jQuery('#edit-submitted-time-hour');
        var $minutes = jQuery('#edit-submitted-time-minute');
        var $datePartySection = jQuery('#webform-component-date-of-party');

    }

    $year.change(function () {
        console.log(get_message('updateHour'));
    });
    $month.change(function () {
        console.log(get_message('updateHour'));
    });
    $date.change(function () {
        console.log(get_message('updateHour'));
    });
    $hour.change(function () {
        console.log(get_message());
    });
    $minutes.change(function () {
        console.log(get_message());
    });
    function get_message(update) {
        today = new Date();
        year = parseInt($year.val());
        month = parseInt($month.val()) - 1;
        date = parseInt($date.val());
        hour = parseInt($hour.val());
        minutes = parseInt($minutes.val());

        if (update == 'updateHour') {
            makeHourSelectElement();
        }

        if (year < 2015 && year > (today.getYear() + 3)) return 'errorDate';
        if (date > 31 && date < 1) return 'errorDate';
        if (month < 1 && month > 12) return 'errorDate';
        if (hour < 11 && hour > 17) return 'errorHour';
        if (minutes < 0 && minutes > 30) return 'errorMinutes';
        var $day = new Date(year, month, date);
        var day = $day.getDay();
        console.log(day, 'day');
        jQuery('#monday-wrong-day').remove();
        jQuery('#form-disable-warning').remove();
        if (day == 1) {
            $datePartySection.append('<div class="form-warning arrow_box" id="monday-wrong-day">' + $mondayText + '</div>');
            jQuery('.contact-form-wrapper input[type=submit]').attr('disabled', true);
        }
        if ($hour.val() == "" && $minutes.val() == "") return;

        hide = hide_rezerwacje_today(day, hour, minutes);

        if (hide == false) {

            jQuery('.webform-component-webform_time').append('<div  class="form-warning arrow_box" id="form-disable-warning">' + $warningText + '</div>');
            jQuery('.contact-form-wrapper input[type=submit]').attr('disabled', true);

        }
        else {
            jQuery('.contact-form-wrapper input[type=submit]').attr('disabled', false);
        }
        return hide;
    }


    if (jQuery('.view-gallery-folders-list > .view-content').length != 0) {
        jQuery('.view-gallery-folders-list > .view-content').packery({
            itemSelector: '.views-row'/*,*/
            // "isHorizontal": true
        });
    }
    if (jQuery('.grid-item').length != 0) {
        $(".grid-item").fancybox({
            helpers: {
                thumbs: {
                    width: 50,
                    height: 50
                }
            }
        });
    }
    // jQuery('.item').click(function(){jQuery(this).toggleClass('is-expanded')});
    if (jQuery('.packery').length != 0) {


        $(".zoom-image-button").fancybox({
            padding: 12,

            helpers: {
                thumbs: {
                    width: 75,
                    height: 75

                },
                buttons: {}
            }
        });

    }

});


