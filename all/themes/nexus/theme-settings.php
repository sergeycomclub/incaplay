<?php 
function themename_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['theme_settings']['your_option'] = array(
    '#type' => 'checkbox',
    '#title' => t('Your Option'),
    '#default_value' => theme_get_setting('your_option'),
  );
}