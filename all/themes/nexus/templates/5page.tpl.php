<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<?php 
$url_behance = "#";
$url_pinterest = "#";
$url_facebook = "#";
$url_twitter = "#";
  $top_banner_url_1 = "#";
  $top_banner_url_2 = "#";
  $top_banner_url_3 = "#";
  $top_banner_url_4 = "#";
  $newsletter="#";
  $rezerwacie="#";
theme_get_setting('your_option');
?>
<section id="page">
<div id="top_search_bar">
      <div class="container">
      <div id="top_search_bar_text">Moc zabawy w magicznym ogrodzie</div>
      <div id="top_search_bar_block">Szukaj<?php print render ($page['top_search_bar']); ?>
      </div>
      </div>
 </div>
 <div id="fixed-left">
   <div id="newsletter">
   <a href="<?php $newsletter ?>">
     <img src="/img/newsletter.png" alt="" class="newsletter-img">
     </a>
   </div>
   <div id="rezerwacie"> 
   <a href="<?php $rezerwacie ?>">
        <img src="/img/rezerwacie.png" alt="" id="img_rezerwacie">
        </a>
   </div>
 </div>
<section id="page">
  <?php if($page['top_bar']): ?>
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php print render ($page['top_bar']); ?>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
  <?php endif; ?> 
  <header id="masthead" class="site-header container" role="banner">
    <div class="row">
      <div id="logo" class="site-branding col-sm-12">
        <?php if ($logo): ?>
          <div id="site-logo">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
          </div>
        <?php endif; ?>
        <h1 id="site-title">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
        </h1>
        <?php if ($site_slogan): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
    <div id="logo_socials" class="col-sm-2 socials">
    <a href="<?php echo $url_behance ?>"><i class="fa fa-behance"></i></a>
    <a href="<?php echo $url_pinterest ?>"><i class="fa fa-pinterest-p"></i> </a>
    <a href="<?php echo $url_facebook ?>"><i class="fa fa-facebook"></i> </a>
    <a href="<?php echo $url_twitter ?>"><i class="fa fa-twitter"></i></div></a>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 mainmenu">
        <nav id="navigation" role="navigation">
          <div id="main-menu">
            <?php 
              if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              } else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
            ?>
          </div>
        </nav>
      </div>
      </div>
  </header>

  <?php if ($is_front): ?>
   <?php if (theme_get_setting('slideshow_display','nexus')): ?>
    <?php
      $slide1_head = check_plain(theme_get_setting('slide1_head','nexus'));   $slide1_desc = check_markup(theme_get_setting('slide1_desc','nexus'), 'full_html'); $slide1_url = check_plain(theme_get_setting('slide1_url','nexus'));
      $slide2_head = check_plain(theme_get_setting('slide2_head','nexus'));   $slide2_desc = check_markup(theme_get_setting('slide2_desc','nexus'), 'full_html'); $slide2_url = check_plain(theme_get_setting('slide2_url','nexus'));
      $slide3_head = check_plain(theme_get_setting('slide3_head','nexus'));   $slide3_desc = check_markup(theme_get_setting('slide3_desc','nexus'), 'full_html'); $slide3_url = check_plain(theme_get_setting('slide3_url','nexus'));
    ?>
    <div id="slidebox" class="flexslider">
      <ul class="slides">
        <li>
          <img class="" src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-1.jpg'; ?>"/>
          <?php if($slide1_head || $slide1_desc) : ?>
            <div class="flex-caption">
              <h2><?php print $slide1_head; ?></h2><?php print $slide1_desc; ?>
              <a class="frmore" href="<?php print url($slide1_url); ?>"> <?php print t('READ MORE'); ?> </a>
            </div>
          <?php endif; ?>
        </li>
        <li>
          <img class="" src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-2.jpg'; ?>"/>
          <?php if($slide2_head || $slide2_desc) : ?>
            <div class="flex-caption">
              <h2><?php print $slide2_head; ?></h2><?php print $slide2_desc; ?>
              <a class="frmore" href="<?php print url($slide2_url); ?>"> <?php print t('READ MORE'); ?> </a>
            </div>
          <?php endif; ?>
        </li>
        <li>
          <img class="" src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-3.jpg'; ?>"/>
          <?php if($slide3_head || $slide3_desc) : ?>
            <div class="flex-caption">
              <h2><?php print $slide3_head; ?></h2><?php print $slide3_desc; ?>
              <a class="frmore" href="<?php print url($slide3_url); ?>"> <?php print t('READ MORE'); ?> </a>
            </div>
          <?php endif; ?>
        </li>
      </ul><!-- /slides -->

      <div class="doverlay"></div>
    </div>
    <?php endif; ?>
  <?php endif; ?>

  <?php if($page['preface_first'] || $page['preface_middle'] || $page['preface_last']) : ?>
    <?php $preface_col = ( 12 / ( (bool) $page['preface_first'] + (bool) $page['preface_middle'] + (bool) $page['preface_last'] ) ); ?>
    <section id="preface-area">
      <div class="container">
        <div class="row">
          <?php if($page['preface_first']): ?><div class="preface-block col-sm-<?php print $preface_col; ?>">
            <?php print render ($page['preface_first']); ?>
          </div><?php endif; ?>
          <?php if($page['preface_middle']): ?><div class="preface-block col-sm-<?php print $preface_col; ?>">
            <?php print render ($page['preface_middle']); ?>
          </div><?php endif; ?>
          <?php if($page['preface_last']): ?><div class="preface-block col-sm-<?php print $preface_col; ?>">
            <?php print render ($page['preface_last']); ?>
          </div><?php endif; ?>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <?php if($page['header']) : ?>
    <section id="header-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php print render($page['header']); ?>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>

<?php 
include dirname(__FILE__)."/top-banner-slider.php"; ?>
  
  <div id="main-content">
  <div class="white-block"></div>
    <div class="container"> 
      <div class="row">
        <?php if(($page['sidebar_first']) && ($page['sidebar_second'])) { 
          $primary_col = 4; 
          $aside_count = 'aside-left aside-right';
        } else if(($page['sidebar_first']) or ($page['sidebar_second'])) { 
          $primary_col = 8; 
          if($page['sidebar_first']) {
            $aside_count = 'aside-left';
          } else {
            $aside_count = 'aside-right';
          }
        } else {
          $primary_col = 12; 
          $aside_count = '';
        } ?>
        <div id="primary" class="content-area col-sm-<?php print $primary_col; ?> <?php print $aside_count; ?>">
          <section id="content" role="main" class="clearfix center">
            <?php if (theme_get_setting('breadcrumbs')): ?><?php if ($breadcrumb): ?><div id="breadcrumbs"><?php print $breadcrumb; ?></div><?php endif;?><?php endif; ?>
            <?php print $messages; ?>
            <?php if ($page['content_top']): ?><div id="content_top"><?php print render($page['content_top']); ?></div><?php endif; ?>
            <div id="content-wrap">
              <?php print render($title_prefix); ?>
              <?php if ($title): ?><h1 class="page-title"><?php print $title; ?></h1><?php endif; ?>
              <?php print render($title_suffix); ?>
              <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
              <?php print render($page['content']); ?>
            </div>
          </section>
        </div>
        <?php if ($page['sidebar_first']): ?>
          <aside id="sidebar-first" class="col-sm-4" role="complementary">
            <?php print render($page['sidebar_first']); ?>
          </aside> 
        <?php endif; ?>
        <?php if ($page['sidebar_second']): ?>
          <aside id="sidebar-second" class="col-sm-4 pull-right" role="complementary">
            <?php print render($page['sidebar_second']); ?>
          </aside> 
        <?php endif; ?>
      </div>
    </div>
  <div class="color-block"></div>
  </div>
  </div>
   
   <section id="third_menu_block"> 
   <div class="container"> 
    <div class="row">
      <div class="col-sm-12 mainmenu">
          <div id="main-menu">
            <?php 
              if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              } else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
            ?>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-sm-6 video_wrapper">

      <iframe width="100%" height="315" src="https://www.youtube.com/embed/YPWizbgu1ww?controls=0&showinfo=0&modestbranding=1&wmode=transparent" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-sm-6 video_text_wrapper">
    TEXT, TEXT, TEXT
      </div>
      </div>
  </section>


  <?php if($page['footer']) : ?>
    <section id="footer-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php print render($page['footer']); ?>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']): ?>
    <?php $footer_col = ( 12 / ( (bool) $page['footer_first'] + (bool) $page['footer_second'] + (bool) $page['footer_third'] + (bool) $page['footer_fourth'] ) ); ?>
    <section id="bottom">
      <div class="container">
        <div class="row">
          <?php if($page['footer_first']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_first']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_second']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_second']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_third']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_third']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_fourth']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_fourth']); ?>
          </div><?php endif; ?>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="fcred col-sm-12">
          <?php print t('Copyright'); ?> &copy; <?php echo date("Y"); ?>, <a href="<?php print $front_page; ?>"><?php print $site_name; ?></a>. <?php print t('Theme by'); ?>  <a href="http://www.incaplay.pl" target="_blank">Incaplay</a>.
        </div>
      </div>
    </div>
  </footer>
</section>
<?php if (theme_get_setting('grid_display','nexus')): ?>
  <div class="button_grid"></div>
<?php endif; ?>

<script type="text/javascript">

function ColorLuminance(color, percent) {
    var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
}


var menuItemNum = jQuery("#block-system-main-menu ul.menu.nav.nav-pills > li.active-trail").index();
var main_content = jQuery("#main-content");
var startColor ="rgb(196, 158, 109)";
    var stopColor = "rgb(150, 116, 83)";
    var buttonColor = "#907554";
switch (menuItemNum) {
  case 0:
  buttonColor ="#30897f";
  startColor = "#53b496";
  stopColor = "#389175";
  break
  case 6 :
  buttonColor="#545c9c";
   startColor ="rgb(115, 122, 210)";
   stopColor = "rgb(80, 87, 156)";
    darker = ColorLuminance("#6269b7", "10")
    jQuery("#block-system-main button").css("background-color", "darker"  );
    jQuery("#block-system-main button").css("border-color", "darker"  );
    
    break


  case 1:
    startColor ="#66BBED";
    stopColor = "#489FE5";
    buttonColor="#4093d8";
    
    break

  case 2:
    startColor ="#B0BCBC";
    stopColor = "#939D9E";
    buttonColor = "#869494";
    jQuery("#color_button").css("background-color", "red !important");
    jQuery("#color_button").css("border-color", "red !important");

   
    break

  case 3:
    darker = ColorLuminance("#7cb338", "0.5");
    console.log(darker);
    startColor ="rgb(144, 200, 76)";
    stopColor = "rgb(103, 158, 35)";
    buttonColor = "#6a953a";
    
    break

    case 4:
    startColor ="rgb(245, 85, 154)";
    stopColor = "rgb(236, 0, 139)";
    buttonColor = "#545c9c";
    
    break

    case 5:
    startColor ="rgb(196, 158, 109)";
    stopColor = "rgb(150, 116, 83)";
    buttonColor = "#907554";
 
    break


  default:
  startColor ="rgb(196, 158, 109)";
  stopColor = "rgb(150, 116, 83)";
  buttonColor = "#907554";

}
 
       jQuery(".color-block").css("border-color", stopColor + " transparent transparent transparent" );
    main_content.css("background-image", "repeating-linear-gradient(to bottom,"+startColor+" 0%, "+stopColor+" 100%)");
 // main_content.css("background-color", stopColor);

 //    // main_content.css("background-image", "url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxIDEiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxsaW5lYXJHcmFkaWVudCBpZD0idnNnZyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSIwJSIgeTE9IjAlIiB4Mj0iMCUiIHkyPSIxMDAlIj48c3RvcCBzdG9wLWNvbG9yPSIjOTBjODRjIiBzdG9wLW9wYWNpdHk9IjEiIG9mZnNldD0iMCIvPjxzdG9wIHN0b3AtY29sb3I9IiM2NzllMjMiIHN0b3Atb3BhY2l0eT0iMSIgb2Zmc2V0PSIxIi8+PC9saW5lYXJHcmFkaWVudD48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI3ZzZ2cpIiAvPjwvc3ZnPg==)");
 //    main_content.css("background-image", "-webkit-gradient(linear, 0% 0%, 0% 100%,color-stop(0, "+colorStart+"),color-stop(1, "+stopColor+"))");

 //    main_content.css("background-image", "-webkit-repeating-linear-gradient(top,"+startColor+" 0%,"+stopColor+" 100%)");

 //    main_content.css("background-image", "-ms-repeating-linear-gradient(top,"+startColor+" 0%,"+stopColor+" 100%)");
    jQuery("#color_button").css('background-color', buttonColor);
    jQuery("#color_button").css('border-color', buttonColor);

</script>




