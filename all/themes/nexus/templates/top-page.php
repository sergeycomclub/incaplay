<?php 
$url = $_SERVER['REQUEST_URI'];
$moc_zabaw_en="The power of fun in a magical garden";
$moc_zabaw = "Moc zabawy w magicznym ogrodzie";
$search_en = "Search";
$search = "Szukaj";

$slider_text_10="Moc atrakcji";
$slider_text_10_en="Power of play";
$slider_text_11 = "w jednym miejscu";
$slider_text_11_en = "in one place";
$slider_text_12 = "Zobacz więcej";
$slider_text_12_en = "Show more";
$slider_text_13 = "Zarezerwuj";
$slider_text_13_en = "Order";
$slider_text_14 = "teraz!";
$slider_text_14_en = "right now!";

$slider_text_20="Znajdziesz nas";
$slider_text_20_en="Locate us";
$slider_text_21 = "w Blue City";
$slider_text_21_en = "in Blue City";
$slider_text_22 = "Sprawdź";
$slider_text_22_en = "Check it";
$slider_text_23 = "Jesteśmy na";
$slider_text_23_en = "We are";
$slider_text_24 = "IV piętrze";
$slider_text_24_en = "on IV floor";

$slider_text_30="kawiarenka";
$slider_text_30_en="cafeteria";


$slider_text_40="Wyjątkowe";
$slider_text_40_en="Exceptional";
$slider_text_41 = "wrodziny";
$slider_text_41_en = "Birthday";
$slider_text_42 = "Sprawdź";
$slider_text_42_en = "Check it";
$slider_text_43 = "Zarezerwuj";
$slider_text_43_en = "Order";
$slider_text_44 = "teraz!";
$slider_text_44_en = "right now!";



$language_var = "";
if(strpos($url,'=en/') !== false || strpos($url,'/en/') !== false){
  $language_var = "en/";
$search = $search_en;
$moc_zabaw = $moc_zabaw_en;
$slider_text_10 = $slider_text_10_en;
$slider_text_11 = $slider_text_11_en;
$slider_text_12 = $slider_text_12_en;
$slider_text_13 = $slider_text_13_en;
$slider_text_14 = $slider_text_14_en;

$slider_text_20 = $slider_text_20_en;
$slider_text_21 = $slider_text_21_en;
$slider_text_22 = $slider_text_22_en;
$slider_text_23 = $slider_text_23_en;
$slider_text_24 = $slider_text_24_en;

$slider_text_30 = $slider_text_30_en;

$slider_text_40 = $slider_text_40_en;
$slider_text_41 = $slider_text_41_en;
$slider_text_42 = $slider_text_42_en;
$slider_text_43 = $slider_text_43_en;
$slider_text_44 = $slider_text_44_en;




}
$url_behance = "#";
$url_pinterest = "#";
$url_facebook = "https://www.facebook.com/INCAPLAY.DLA.DZIECI";
$url_twitter = "#";
  $top_banner_url_1 = "/".$language_var."atrakcje-dla-dzieci";
  $top_banner_url_2 = "/".$language_var."kontakt#map";
  $top_banner_url_3 = "/".$language_var."inca-cafe";
  $top_banner_url_4 = "/".$language_var."rezerwacje";
  $newsletter="/".$language_var."subscribe";
  $rezerwacie="/".$language_var."rezerwacje";
theme_get_setting('your_option');

function slideshow_incaplay($node, $field_name){


$gallery_fields = field_get_items('node', $node, $field_name);

if (!empty($gallery_fields)) :
?>
<div class="slider-wrapper col-md-6 col-sm-12"> 
<div class="flexslider">
  <ul class="slides">
  <?php
  foreach($gallery_fields as $gallery_item) { 
    ?>
<li>
  <?php
    $image_item = array(
      'style_name' => 'flexslider_full', // just enter the sytle name you'd like
      'path' => $gallery_item['uri'],
      'width' => '',
      'height' => '',
      'alt' => $gallery_item['alt'], // optional
      'title' => $gallery_item['title'], // optional
    );

    print theme('image_style', $image_item);?>
</li>
    <?php
  }
  ?>
  </ul>
</div>
</div>
  <?php
  
endif;
}

?>
<section id="page">
<div id="top_search_bar">
      <div class="container">
      <div id="top_search_bar_text"><?php echo $moc_zabaw ; ?> </div>
      <div id="top_search_bar_block"><?php //echo $search . " " ; ?> <?php print render ($page['top_search_bar']); ?>
      </div>
      </div>
 </div>
 <div id="fixed-left">
  <div class="fixed-wrapper">
            <div id="rezerwacie"> 
           <a href="<?php echo $rezerwacie; ?>">
                <span  id="img_rezerwacie">Rezerwacje<span id='urodzinowe'><br>urodzinowe</span></span>
                </a>
           </div>
            <div id="newsletter-wrapper">

                <!-- div class="newsletter_logo"><span clas="first_letter">N</span>ewsletter</div> -->
                <span  id="newsletter-span">
                <div class="newsletter-form-wrapper">
                <? include_once 'subscribe-pl.php';?>
                </div> 
                <a class="white-color-link" href="<?php echo $newsletter; ?>">Newsletter</a></span>
            </div>
  </div>
 </div>
<section id="page">
  <?php if($page['top_bar']): ?>
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php print render ($page['top_bar']); ?>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
  <?php endif; ?> 
  <header id="masthead" class="site-header container" role="banner">
    <div class="row">
          <i class="fa fa-bars"></i>
      <div id="logo" class="site-branding col-sm-12">
        <?php if ($logo): ?>
          <div id="site-logo">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
          </div>
        <?php endif; ?>
    <div id="logo_socials" class="col-sm-3 socials">
    <!-- <a href="<?php echo $url_behance ?>"><i class="fa fa-behance"></i></a> -->
    <!-- <a href="<?php echo $url_pinterest ?>"><i class="fa fa-pinterest-p"></i> </a> -->
    <a href="<?php echo $url_facebook ?>"><i class="fa fa-facebook"></i> </a>
<!--     <a href="<?php echo $url_twitter ?>"><i class="fa fa-twitter"></i></div></a> -->
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 mainmenu">
        <nav id="navigation" role="navigation">
          <div id="main-menu">
            <?php 
              if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              } else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
            ?>
          </div>
        </nav>
      </div>
      </div>
  </header>
<div id="sticky-menu">
  <div class="container">
    <div class="col-sm-1"><a href="/"><img src="<?php print $logo; ?>" alt="" class="logo"></a></div>
    <div class="col-sm-11">   <nav id="navigation-sticky" role="navigation">
            <div id="main-menu-sticky">
              <?php 
                if (module_exists('i18n_menu')) {
                  $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
                } else {
                  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
                }
                print drupal_render($main_menu_tree);
              ?>
            </div>
          </nav>
          </div>
  </div>
</div>
<?php 
if (drupal_is_front_page() || $_SERVER['REQUEST_URI'] == '/en/about-us')  include dirname(__FILE__)."/top-banner-slider.php"; ?>
<div id="contacts-address"></div>

  