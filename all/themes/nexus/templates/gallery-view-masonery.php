<script src="/sites/all/modules/packery/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox.pack.js"></script>
<!-- <script type="text/javascript" src="/sites/default/files/fancybox/jquery.easing.1.3.js "></script> -->
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-media.js"></script>
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />

<?
$gallery_folder_uri = '/sites/default/files/gallery_folder/';
?>
<div class="grid js-masonry"  data-masonry-options='{ "itemSelector": ".grid-item", "columnWidth": ".grid-item", "gutter": 5 }'>
<div class="grid-sizer"></div>
	<? foreach ($node->field_obraz_do_galerii['und'] as $key => $image) :?>


	<a href="<?=$gallery_folder_uri.$image['filename']?>" class="grid-item <? if(rand(0,1)==1){ echo 'grid-item-width2'; } ?> zoom-image-button" rel="fancybox-thumb">
		<img   src="<?=$gallery_folder_uri.$image['filename']?>">
	</a>


	<?endforeach;?>
</div>
