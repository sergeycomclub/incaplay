<?php 

 ?>
<div class="container top-banner-slider-container">
        <div class="row no-gutter">
          <div class="col-sm-12 banner-top no-gutter" id="banner-top">

	      <!-- First coloumn -->	        
          <div class="col-xs-6 col-sm-6 col-md-3  top-baner-col" id="top-baner-col-1">
          <a href="<?php echo $top_banner_url_1; ?>">
	          <div class="top">
		          <div class="text-wrapper-overflow-hidden">
			          <div class="slide1">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_10 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_11 ; ?></span>
				          </h2>
				          <span class="fancy">
				          <span class="banner-top-text "><?php echo $slider_text_12 ; ?></span>
				          </span>
			          </div>
  			          <div class="slide2">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_13 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_14 ; ?></span>
				          </h2>
				          <hr class="crossed">
				          
			          </div>

		          </div>
	          </div>
	          <div class="bottom">
	           <div id="cf">
		          <img src="/img/placeholder11.jpg" alt="" class="image-banner-bottom">
		          <img src="/img/placeholder10.jpg" alt="" class="image-banner-top">
		       </div>
	          </div>
          </a>
          </div>
  		  <!-- End of First coloumn -->      
  	      <!-- Second coloumn -->	        
          <div class="col-xs-6 col-sm-6 col-md-3  top-baner-col" id="top-baner-col-2">
	          <a href="<?php echo $top_banner_url_2 ?>">
	          <div class="top">
		          <div class="text-wrapper-overflow-hidden">
			          <div class="slide1">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_20 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_21 ; ?></span>
				          </h2>
				          <span class="fancy">
				          <span class="banner-top-text "><?php echo $slider_text_22 ; ?></span>
				          </span>
			          </div>
  			          <div class="slide2">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_23 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_24 ; ?></span>
				          </h2>
				          <hr class="crossed">
				          
			          </div>

		          </div>
	          </div>
	          <div class="bottom">
	           <div id="cf">
		          <img src="/img/placeholder21.jpg" alt="" class="image-banner-bottom">
		          <img src="/img/placeholder20.jpg" alt="" class="image-banner-top">
		        </div>
	          </div>
	          </a>
          </div>
  		  <!-- End of First coloumn -->              
	      <!-- Third coloumn -->	        
          <div class="col-xs-6 col-sm-6 col-md-3  top-baner-col" id="top-baner-col-3">
	         <a href="<?php echo $top_banner_url_3 ?>">
	          <div class="bottom">
	           <div id="cf">
		          <img src="/img/placeholder31.jpg" alt="" class="image-banner-bottom">
		          <img src="/img/placeholder30.jpg" alt="" class="image-banner-top">
		       </div>
	          </div>
	          	          <div class="top">
		          <div class="text-wrapper-overflow-hidden">
			          <div class="slide1">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big">Inca Cafe</span><br/>
					      </h2>
				          <hr class="crossed">
				          <span class="banner-top-text "><?php echo $slider_text_30 ; ?></span>
					          <br/><br/>
				      </div>
  			          <div class="slide2">	
				          <h2 class="banner-top">
					          <hr class="crossed">
					          <span class="top-banner-h2-big">Menu</span><br/>
					          <hr class="crossed">
				          </h2>
			          </div>

		          </div>
	          </div>
	          </a>
          </div>
  		  <!-- End of Third coloumn -->
	      <!-- Four coloumn -->	        
          <div class="col-xs-6 col-sm-6 col-md-3  top-baner-col" id="top-baner-col-4">
	          <a href="<?php echo $top_banner_url_4 ?>">
	          <div class="top">
		          <div class="text-wrapper-overflow-hidden">
			          <div class="slide1">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_40 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_41 ; ?></span>
				          </h2>
				          <span class="fancy">
				          <span class="banner-top-text "><?php echo $slider_text_42 ; ?></span>
				          </span>
			          </div>
  			          <div class="slide2">	
				          <h2 class="banner-top">
					          <span class="top-banner-h2-big"><?php echo $slider_text_43 ; ?></span><br/>
					          <span class="top-baner-h2-small"><?php echo $slider_text_44 ; ?></span>
				          </h2>
				          <hr class="crossed">
				          
			          </div>

		          </div>
	          </div>
	          <div class="bottom">
	          <div id="cf">
		          <img src="/img/placeholder41.jpg" alt="" class="image-banner-bottom">
		          <img src="/img/placeholder40.jpg" alt="" class="image-banner-top">
		       </div>
	          </div>
	          </a>
          </div>
  		  <!-- End of Fourth coloumn -->
	      

  </div>
  </div>
 
    </div>
    <?php ?>