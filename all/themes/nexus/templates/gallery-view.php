<script src="/sites/all/modules/packery/packery.pkgd.min.js"></script>
<script src="/sites/all/modules/packery/draggabilly.pkgd.min.js"></script>
<script src="/sites/all/modules/packery/jquery-ui.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox.pack.js"></script>
<!-- <script type="text/javascript" src="/sites/default/files/fancybox/jquery.easing.1.3.js "></script> -->
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="/sites/default/files/fancybox/jquery.fancybox-media.js"></script>
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/sites/default/files/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />

<?
$gallery_folder_uri = '/sites/default/files/gallery_folder/';
?>
<div class="packery">

	<? foreach ($node->field_obraz_do_galerii['und'] as $key => $image) :?>

	<div class="item">
		<div class="item-content">
				<img   src="<?=$gallery_folder_uri.$image['filename']?>">
				
			<a href="<?=$gallery_folder_uri.$image['filename']?>" class="zoom-image-button" rel="fancybox-thumb">
			</a>
		</div>
	</div>

	<?endforeach;?>
</div>
<script type="text/javascript">
	var $container = jQuery('.packery').imagesLoaded( function() {

		$container.packery({
			itemSelector: '.item',
			width: 60/*,*/
      // "isHorizontal": true
  });

  // get item elements, jQuery-ify them
  var $itemElems = $container.find('.item');
  // make item elements draggable
  $itemElems.draggable();
  // bind Draggable events to Packery
  $container.packery( 'bindUIDraggableEvents', $itemElems );

	});
</script>