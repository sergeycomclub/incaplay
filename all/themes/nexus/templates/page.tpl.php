<?php include_once("analyticstracking.php") ?>
<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */

?>
<?
$current_url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; 
$show_h1_title = true;
$hide_h1_title_field_value = false;
if (isset( $node ) && $node->type == "two_content_areas"){
  $hide_h1_title_field = field_get_items('node', $node, 'field_poka_h1_title');
  $hide_h1_title_field_value = $hide_h1_title_field[0]['value'];
}
else{
  $hide_h1_title_field_value = 1;
}

$hide_h1_title_field_value = ($hide_h1_title_field_value == 1)? true: false;
$show_h1_title = $show_h1_title && !$hide_h1_title_field_value;
?>

<?php 

include dirname(__FILE__)."/top-page.php"; ?>

<div id="main-content" class="<?php if(isset( $node) && $node->type == 'article') {echo 'article';} ?>">
  <div class="white-block"></div>
  <div class="container"> 
    <div class="row"> 
      <?php if(($page['sidebar_first']) && ($page['sidebar_second'])) { 
        $primary_col = 4; 
        $aside_count = 'aside-left aside-right';
      } else if(($page['sidebar_first']) or ($page['sidebar_second'])) { 
        $primary_col = 8; 
        if($page['sidebar_first']) {
          $aside_count = 'aside-left';
        } else {
          $aside_count = 'aside-right';
        }
      } else {
        $primary_col = 12; 
        $aside_count = '';
      } ?>
      <div id="primary" class="content-area col-sm-<?php print $primary_col; ?> <?php print $aside_count; ?>">
        <section id="content" role="main" class="clearfix center">
          <?php if (theme_get_setting('breadcrumbs')): ?><?php if ($breadcrumb): ?><div id="breadcrumbs"><?php print $breadcrumb; ?></div><?php endif;?><?php endif; ?>
          <?php print $messages; ?>
          <?php if ($page['content_top']): ?><div id="content_top"><?php print render($page['content_top']); ?></div><?php endif; ?>
          <div id="content-wrap">
            <?php  

            if (isset( $node )){
              $field_address_area = field_get_items('node', $node, 'field_address_area');
              $field_address_area = $field_address_area[0]['value'];
              
              print $field_address_area;
              $field_address_area = field_get_items('node', $node, 'field_secondary_area');
              $field_secondary_area = $field_address_area[0]['value'];
              
              print $field_secondary_area;
            }

            ?>

          </div>
        </section>
      </div>
      <?php if ($page['sidebar_first']): ?>
        <aside id="sidebar-first" class="col-sm-4" role="complementary">
          <?php print render($page['sidebar_first']); ?>
        </aside> 
      <?php endif; ?>
      <?php if ($page['sidebar_second']): ?>
        <aside id="sidebar-second" class="col-sm-4 pull-right" role="complementary">
          <?php print render($page['sidebar_second']); ?>
        </aside> 
      <?php endif; ?>
    </div>
  </div>
  <div class="color-block"></div>
</div>
</div>

<section id="third_menu_block" class="<?php if(isset( $node) && $node->type == 'article') {echo 'article';} ?>"> 
 <div class="container"> 
   <? if ($show_h1_title):?>
   <div class="row">
     <a class="no-decoration" href="<?=$current_url?>">
       <h1 class="page-title title no-bold capitalized"><?php print $title; $title_shown = true;?></h1>
     </a>
   </div>
   <?endif;?>
   <div class="row">
    <?php
    if (isset( $node )){
      $bodycontent = field_get_items('node', $node, 'body');
      $bodycontent = $bodycontent[0]['value'];
    //var_dump($_SERVER['REQUEST_URI'] !='/?q=en/rezerwacje');
      if( $node->type == "article"): ?>
      <div class="container"> 
        <? if ($show_h1_title):?>
        <div class="row">
          <a class="no-decoration" href="<?=$current_url?>">
            <h1 class="page-title"><?php if (!$title_shown){ print $title;} ?></h1>
          </a>
        </div>
        <?endif;?>
      </div>
    <?php endif; 
    if ($node->type == 'gallery_folder'){
                // include_once 'gallery-view-masonery.php';
      include_once 'gallery-view.php';

    }
    if( $node->type == "page" ){
     slideshow_incaplay($node, 'field_slideshow_basic');
   }
   if( $node->type == "two_content_areas" ){
     slideshow_incaplay($node, 'field_slideshow');
   }

   if($_SERVER['REQUEST_URI'] =='/pl/rezerwacje' || 
    $_SERVER['REQUEST_URI'] =='/rezerwacje' || 
    $_SERVER['REQUEST_URI'] =='/en/rezerwacje' || 
    $_SERVER['REQUEST_URI'] =='/en/rezerwacje' ||
    $_SERVER['REQUEST_URI'] =='/en/rezerwacje-en'){
    echo '<div class="contact-form-wrapper form-green-color">';
  print render($page['content']);
  echo '</div>';
}
else {
  print $bodycontent;
            //    print render($page['content']);
}
                }else { //node conditional

                  print render($page['content']);
                }

                ?>

                <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
                <?php print render($page['help']); ?>


              </div>
            </div>
          </section>

          <?php 

          if($page['pre_footer_block']): ?><div class="footer-block col-sm-12">
          <section id="footer-block">
            <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <?php print render ($page['pre_footer_block']); ?>
                </div>
              </div>
            </div>
          </div><?php endif; ?>

          <?php if($page['footer']) : ?>
            <section id="footer-block">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <?php print render($page['footer']); ?>
                  </div>
                </div>
              </div>
            </section>
          <?php endif; ?>


          <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']): ?>
            <?php $footer_col = ( 12 / ( (bool) $page['footer_first'] + (bool) $page['footer_second'] + (bool) $page['footer_third'] + (bool) $page['footer_fourth'] ) ); ?>
            <section id="bottom">
              <div class="container">
                <div class="row">
                  <?php if($page['footer_first']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
                    <?php print render ($page['footer_first']); ?>
                  </div><?php endif; ?>
                  <?php if($page['footer_second']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
                    <?php print render ($page['footer_second']); ?>
                  </div><?php endif; ?>
                  <?php if($page['footer_third']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
                    <?php print render ($page['footer_third']); ?>
                  </div><?php endif; ?>
                  <?php if($page['footer_fourth']): ?><div class="footer-block col-sm-<?php print $footer_col; ?>">
                    <?php print render ($page['footer_fourth']); ?>
                  </div><?php endif; ?>
                </div>
              </div>
            </section>
          <?php endif; ?>
          <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="container">
              <div class="row">
                <div class="fcred col-sm-12">
                  <?php print t('Copyright'); ?> &copy; <?php echo date("Y"); ?>, <a href="<?php print $front_page; ?>"><?php print $site_name; ?></a>. <?php print t('Theme by'); ?>  <a href="http://www.incaplay.pl" target="_blank">Incaplay</a>.
                </div>
              </div>
            </div>
          </footer>
        </section>
        <?php if (theme_get_setting('grid_display','nexus')): ?>
          <div class="button_grid"></div>
        <?php endif; ?>


        <script src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/js/theme.js'; ?>"></script>


        <script type="text/javascript">
          jQuery(window).load(function() {
            jQuery('.flexslider').flexslider({

              animation: "slide",
              autoplay: "true"
            });
          });
        </script>
        